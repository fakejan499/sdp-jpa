package com.example.jpa.demo.employe;

import com.example.jpa.demo.task.Task;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    private String name;

    //Some other code

    @ManyToMany(cascade = CascadeType.REMOVE)
    private List<Task> tasks;

    public Employee(String name){
        this.name = name;
    }
}
