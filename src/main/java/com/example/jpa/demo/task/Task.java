package com.example.jpa.demo.task;

import com.example.jpa.demo.employe.Employee;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    //Some other code

    @ManyToMany(mappedBy = "tasks")
    private List<Employee> employees;
}
