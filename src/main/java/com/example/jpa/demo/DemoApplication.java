package com.example.jpa.demo;

import com.example.jpa.demo.employe.Employee;
import com.example.jpa.demo.employe.EmployeeRepository;
import com.example.jpa.demo.student.StudentRepository;
import com.example.jpa.demo.task.Task;
import com.example.jpa.demo.task.TaskRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.persistence.EntityManager;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootApplication
@Slf4j
public class DemoApplication implements CommandLineRunner {

	@Autowired
	StudentRepository studentRepository;

	@Autowired
	EmployeeRepository employeeRepository;

	@Autowired
	TaskRepository taskRepository;


	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Override
	public void run(String... args) {
		List<Task> tasks1 = Arrays.asList(new Task(), new Task());
		Employee e1 = new Employee("e1");
		e1.setTasks(tasks1);
		taskRepository.saveAll(tasks1);
		employeeRepository.save(e1);

		List<Task> tasks2 = Arrays.asList(new Task(), new Task());
		Employee e2 = new Employee("e2");
		e2.setTasks(tasks2);
		taskRepository.saveAll(tasks2);
		employeeRepository.save(e2);

		// Po tej operacji usunięte zostaną również taski pracownika.
		employeeRepository.delete(e1);

		e2.setName("employee");
		employeeRepository.saveAndFlush(e2);
	}
}
